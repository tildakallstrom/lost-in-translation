# Lost in translation

React app with login system
Translates messages to sign language characters (us)

# Usage

- Create a file in root folder and call it '.env' and insert following lines:
    REACT_APP_API_KEY=<key>
    REACT_APP_API_URL=<url>

and enter your API key and URL in place of <key> and <url>.
Access key and URL with 'process.env.REACT_APP_API_KEY' or '[...]_URL'